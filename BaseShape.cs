﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace json
{

    [JsonConverter(typeof(StringEnumConverter))]
    public enum ShapesEnum
    {
        [EnumMember(Value = "point")]
        Point,
        [EnumMember(Value = "area")]
        Area,
        [EnumMember(Value = "line")]
        Line
    }

    public struct Coords
    {
        public float latitude;
        public float longitude;
    }

    [JsonConverter(typeof(ShapeConverter))]
    public class BaseShape
    {
        [JsonProperty(Order = -2)]
        public int id;

        [JsonProperty("type", Order = -2)]
        public ShapesEnum shapeType;
    }
}
