﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;

namespace json
{
    class ShapeConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(BaseShape);
        }

        public override bool CanWrite
        {
            get { return false; }
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new Exception();
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
           
            var item = JObject.Load(reader);
            BaseShape target = null;

            switch (item["type"].Value<string>()) // this is the property differentiater
            {
                case "point":
                    var point = new PointShape();
                    point.id = item["id"].Value<int>();
                    point.coords = item["coords"].ToObject<Coords>();
                    target = point;
                    break;
                case "area":
                    var area = new AreaShape();
                    area.id = item["id"].Value<int>();
                    area.points = item["points"].ToObject<List<PointShape>>();
                    target = area;
                    break;
                case "line":
                    var line = new LineShape();
                    line.id = item["id"].Value<int>();
                    line.from = item["from"].ToObject<PointShape>();
                    line.to = item["to"].ToObject<PointShape>();
                    target = line;
                    break;
                default:
                    throw new NotImplementedException();
            }
            return target;
        }
    }
}
