﻿using System;
using System.Collections.Generic;
using System.Text;

namespace json
{
    class AreaShape : BaseShape
    {
        public List<PointShape> points;

        public AreaShape()
        {
            shapeType = ShapesEnum.Area;
        }
    }
}
