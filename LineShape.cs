﻿using System;
using System.Collections.Generic;
using System.Text;

namespace json
{
    class LineShape : BaseShape
    {
        public PointShape from;
        public PointShape to;

        public LineShape()
        {
            shapeType = ShapesEnum.Line;
        }
    }
}
