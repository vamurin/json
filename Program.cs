﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;

namespace json
{
    class Program
    {
        static void Main(string[] args)
        {
            var point = new PointShape()
            {
                id = 1,
                coords = new Coords
                {
                    latitude = 10f,
                    longitude = 20f
                }
            };

            var area = new AreaShape()
            {
                id = 8,
                points = new List<PointShape>()
                {
                    new PointShape()
                    {
                        id = 3,
                        coords = new Coords
                        {
                            latitude = 30f,
                            longitude = 40f
                        }
                    }
                }
            };

            var line = new LineShape()
            {
                id = 6,
                from = new PointShape()
                {
                    id = 2,
                    coords = new Coords
                    {
                        latitude = 50f,
                        longitude = 60f
                    }
                },
                to = new PointShape()
                {
                    id = 4,
                    coords = new Coords
                    {
                        latitude = 70f,
                        longitude = 80f
                    }
                }
            };

            var jsonOut = JsonConvert.SerializeObject(
                new BaseShape[] { point, area, line },
                new JsonSerializerSettings
                {
                    Formatting = Formatting.Indented
                });

            
            File.WriteAllText("./output.json", jsonOut);

            var jsonIn = File.ReadAllText("./file.json");
            
            var shapes = JsonConvert.DeserializeObject<List<BaseShape>>(jsonIn);

            jsonOut = JsonConvert.SerializeObject(
                shapes,
                new JsonSerializerSettings
                {
                    Formatting = Formatting.Indented
                });

            Console.WriteLine(jsonOut);
        }
    }
}
