﻿using System;
using System.Collections.Generic;
using System.Text;

namespace json
{
    class PointShape : BaseShape
    {
        public Coords coords;

        public PointShape()
        {
            shapeType = ShapesEnum.Point;
        }
    }
}
